# Caffeinate

Prevent the system from sleeping.

Credits to [superuser.com/questions/329758/how-can-i-prevent-a-policy-enforced-screen-lock-in-windows/1596783#1596783](https://superuser.com/questions/329758/how-can-i-prevent-a-policy-enforced-screen-lock-in-windows/1596783#1596783).

## Usage

```bat
caffeinate.cmd
```